import React, { useState } from "react";

//solution 1

// function App() {
//     const [inputText, setInputText] = useState('');
//     const [items, setItems] = useState([])

//     function handleChange(event) {
//         const newValue = event.target.value;
//         setInputText(newValue);
//     }
//     function addItem() {
//         setItems((prevItems) => {
//             return [
//                 ...prevItems, inputText
//             ];
//         })
//         setInputText('');
//     }

//   return (
//     <div className="container">
//       <div className="heading">
//         <h1>To-Do List</h1>
//       </div>
//       <div className="form">
//         <input type="text" value={inputText} onChange={handleChange}/>
//         <button onClick={addItem}>
//           <span>Add</span>
//         </button>
//       </div>
//       <div>
//         <ul>
//         {
//             items.map(item => {
//                 return (
//                     <li>{item}</li>
//                 )
//             })
//         }
//         </ul>
//       </div>
//     </div>
//   );
// }
// export default App;

//solution 2

let list = [];

function App() {
  const [todoListName, setTodoListName] = useState("");
  const [listItems, setListItems] = useState([]);

  
  
  function addList(e) {
    
    list.push(todoListName);
    setListItems(list);
    setTodoListName('');
  }

  return (
    <div className="container">
      <div className="heading">
        <h1>To-Do List</h1>
      </div>
      <div className="form">
        <input
          value={todoListName}
          onChange={(e) => setTodoListName(e.target.value)}
          type="text"
        />
        <button onClick={addList}>
          <span>Add</span>
        </button>
      </div>
      <div>
        <ul>
        {
            listItems.map(listItem => {
                return(
                     <li>{listItem}</li>
                )
               
            })
        }
        </ul>
      </div>
    </div>
  );
}

export default App;
