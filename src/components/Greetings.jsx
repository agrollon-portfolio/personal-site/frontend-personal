import React from 'react'

export default function Greetings() {
    let currentTime = new Date(2019,1,1,18).getHours();

    let greeting = '';
    const customStyle = {
        color: ''
    }

    if(currentTime < 12)
    {
        greeting = 'Good Morning';
        customStyle.color = 'red';
    } else if (currentTime < 18)
    {
        greeting = 'Good Afternoon';
        customStyle.color ='blue';
    } else
    {
        greeting = 'Good Evening';
        customStyle.color = 'green';
    }
    return (
        <h1 className='heading' style={ customStyle }>
            { greeting }
        </h1>
    )
}