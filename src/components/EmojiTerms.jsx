import React from "react";
import emoji from "../data/emojipedia";

function EmojiTerms(props) {
  console.log(emoji);
  return (
    <div className="term">
      <dt>
        <span className="emoji" role="img" aria-label="Tense Biceps">
          {props.emoji}
        </span>
        <span>{props.name}</span>
      </dt>
      <dd>{props.meaning}</dd>
    </div>
  );
}

export default EmojiTerms;
