import React from 'react'

export default function Footer() {
    const fName = "Areej Gabrielle";
    const lName = "Rollon";
    const year = new Date().getFullYear();
    
    return (
        <footer>Created by { fName + ' ' + lName } Copyright { year }</footer>
    )
}